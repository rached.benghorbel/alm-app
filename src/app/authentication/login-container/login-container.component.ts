import { Component, OnInit } from '@angular/core';
import {CredentialsModel} from "../model/credentials.model";

@Component({
  selector: 'app-login-container, [app-login-container]',
  templateUrl: './login-container.component.html'
})
export class LoginContainerComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  authenticate(credentials: CredentialsModel): void {
    console.log(credentials);
  }

}
