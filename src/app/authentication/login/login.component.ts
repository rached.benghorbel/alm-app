import {ChangeDetectionStrategy, Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {CredentialsModel} from '../model/credentials.model';

@Component({
  selector: 'app-login, [app-login]',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  // We set the detection strategy On Push for dump components
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoginComponent implements OnInit {

  authForm: FormGroup;
  @Output() authenticate = new EventEmitter<CredentialsModel>();

  constructor(private readonly formBuilder: FormBuilder) {
    this.authForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  ngOnInit(): void {
  }

  submitAuth(): void {
    if(!this.authForm.valid) {
      this.authForm.markAllAsTouched();
      return;
    }
    this.authenticate.emit(new CredentialsModel(this.authForm.value));
  }

  get username() {
    return this.authForm.get('username')
  }

  get password() {
    return this.authForm.get('password');
  }

}
