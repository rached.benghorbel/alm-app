/**
 * @CredentialsModel is the object to be prepared data in form
 * @username login introduced by the user
 * @password the password introduced by the user
 */
export class CredentialsModel {
  username?: string;
  password?: string;

  constructor(credentials: Partial<CredentialsModel>) {
    Object.assign(this, credentials);
  }

}
